

var mySrcImg = new Image();

var s;

function addLoadEventToFileSelect() {
	document.getElementById("files").addEventListener('change', loadFile, false);
}


function loadFile(evt) {
	var files = evt.target.files;
	var fileSelected = files[0];
	if (!fileSelected.type.match('image.*')) {
		return;
	}

	var reader = new FileReader();
	reader.onload = (function (theFile) {
		return function (e) {
			document.getElementById("outputImage").innerHTML =
			"<img src=\"" + e.target.result + "\" alt=\"Image from file\" id=\"imgOutput\" height=\"200\" width=\"200\">";

		};
	})(fileSelected);

	reader.readAsDataURL(fileSelected);
}

function srcImg() {
	var tmpImg = document.getElementById("imgOutput");
	mySrcImg.src = tmpImg.src;
	
	
}
window.onclick = changeBackgroundColour;

// I have added a function so that the background colour changes from purple(original)to green once whenever the background is clicked.

function changeBackgroundColour() {
    document.getElementsByTagName("BODY")[0].style.backgroundColor = "green";	
}



// This is the first image effect. This is greyscale. 

function greyImg() {
		var canvas = document.getElementById("canvas");
		var context = canvas.getContext("2d")
		// This ensures the data is in the right context
		if (!context || !context.getImageData || !context.putImageData || !context.drawImage) {
			return;
		}

		context.fillStyle = "rgb(0,200,0)";
		context.fillRect(0, 0, 300, 300);
		context.drawImage(mySrcImg, 0, 0);

		// This defines the image data
		var myImageData;
			
	// The image data is the collected.
		try {
			myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		} catch (e) {
			myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		}
		// The slide is then enabled work within the function 
		 s = document.getElementById("sliderBar");
		// This loops over every pixel set on the canvas
		var pixelComponents = myImageData.data;
		var n = pixelComponents.length;
		for (var i = 0; i < n; i += 4) {
			
			var average = (pixelComponents[i] + pixelComponents[i + 1] + pixelComponents[i + 2]) / s.value;

			pixelComponents[i] = average;
			pixelComponents[i + 1] = average;
			pixelComponents[i + 2] = average;
		}
		
		// This then allows the image data to be drawn back onto the canvas
		context.putImageData(myImageData, 0, 0);
}

	
	
	
	
	


// This is the Blur effect function.
function blurImg(){
// This is the convolution width and height.
var convolutionWidth = 3;
var convolutionHeight = 3;

var factor = 1.0;
var bias = 0.0;

//This is the convolution mask 
var convolutionMask = new Array(convolutionHeight);
	for (i=0; i < convolutionHeight; i++){
	convolutionMask[i]=new Array(convolutionWidth)
}

convolutionMask[0][0] = 0.1111111111;		convolutionMask[1][0] = 0.1111111111;		convolutionMask[2][0] = 0.1111111111;
convolutionMask[0][1] = 0.1111111111;		convolutionMask[1][1] = 0.1111111111;		convolutionMask[2][1] = 0.1111111111;
convolutionMask[0][2] = 0.1111111111;		convolutionMask[1][2] = 0.1111111111;		convolutionMask[2][2] = 0.1111111111;
// The end of the convolution mask

// This is the output of the convolution output which is width * height in size.
var convolutionKernel_OutputArraySize =((convolutionWidth * convolutionHeight) * 4);
var convolutionKernel_Output = new Array(convolutionKernel_OutputArraySize);
// This is the end to the convolution kernel output.

	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d"); 
	
	context.fillStyle = "rgb(0,200,0)";
	context.fillRect (0, 0, 300, 300);
	
	// This copies the data to the canvas
	context.drawImage(mySrcImg, 0, 0);
	
	// This defines the image data
	var myImageData;
	var newImageData;
	
	// The image data is the collected.
	try {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		newImageData = context.createImageData(canvas.width, canvas.height);
	} catch (e) {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		newImageData = context.createImageData(canvas.width, canvas.height);
	}
	
	// This loops over every pixel set on the canvas
	for (var x = 0; x < canvas.width; x++){
		for (var y = 0; y < canvas.height; y++) {
		
			// This is the index
			var idx = (x + y * canvas.width) * 4;
			
			// These are the indexes for the convolution kernel 
			for (var filterx = 0; filterx < convolutionWidth; filterx++){
			for (var filtery = 0; filtery < convolutionHeight; filtery++){
				 var tmpX = ((x - Math.floor(convolutionWidth / 2)) + filterx + canvas.width) % canvas.width;
				 var tmpY = ((y - Math.floor(convolutionHeight / 2)) + filtery + canvas.height) % canvas.height;
				 var convolutionKernel_Index = (tmpX + tmpY * canvas.width) * 4;
				 
				 var outputIndex = (filterx + filtery * convolutionWidth) * 4;
				 convolutionKernel_Output[outputIndex  ] = (convolutionMask[filterx][filtery]	* factor)  *
																		myImageData.data[convolutionKernel_Index  ];
				 convolutionKernel_Output[outputIndex+1] = (convolutionMask[filterx][filtery]	* factor)  *
																		myImageData.data[convolutionKernel_Index+1];
				 convolutionKernel_Output[outputIndex+2] = (convolutionMask[filterx][filtery]	* factor)  *
																		myImageData.data[convolutionKernel_Index+2];
				 convolutionKernel_Output[outputIndex+3] = 255;
			
	// This then loops through the output values 		
	var newPixel = new Array(4);
	for (i=0; i < 4; i++){
		newPixel[i] = 0; 
	}
	
	for (i=0; i < convolutionKernel_OutputArraySize; i+=4){
		newPixel[0] = newPixel[0] + convolutionKernel_Output[i  ] + bias;
		newPixel[1] = newPixel[1] + convolutionKernel_Output[i+1] + bias;
		newPixel[2] = newPixel[2] + convolutionKernel_Output[i+2] + bias;
		newPixel[3] = newPixel[3] + convolutionKernel_Output[i+3] + bias;
	}
// This sets the new pixel colour
	newImageData.data[idx  ] = Math.min( Math.max(newPixel[0], 0), 255);
	newImageData.data[idx+1] = Math.min( Math.max(newPixel[1], 0), 255);
	newImageData.data[idx+2] = Math.min( Math.max(newPixel[2], 0), 255);
	newImageData.data[idx+3] = Math.min( Math.max(newPixel[3], 0), 255);
			

	}
}

}
}
// This then allows the image data to be drawn back onto the canvas
context.putImageData(newImageData, 0, 0);
}



// This is the Hue effect function 
function hueImg() {
	
	// This defines the canvas
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	
	// This calls the image data onto the canvas
	context.fillStyle = "rgb(0,200,0)";
	context.fillRect (0, 0, 300, 300);
	context.drawImage(mySrcImg, 0, 0);
	
	// This defines the image data
	var myImageData; 
	
	// The image data is the collected.
	try {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
	} catch (e) {
	
		
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
	}
	// The slide is then enabled work within the function 
	s = document.getElementById("sliderBar");
	
	// This loops over every pixel set on the canvas
	var pixelComponents = myImageData.data;
	var n = pixelComponents.length;
	for (var i = 0; i < n; i += 4){
	
		pixelComponents[i  ] = pixelComponents[i  ] + (s.value*10); 
		pixelComponents[i+1] = pixelComponents[i+1];
		pixelComponents[i+2] = pixelComponents[i+2];
	
	}
	
	// This then allows the image data to be drawn back onto the canvas
	context.putImageData(myImageData, 0,0);
}


// This is the invert effect function 

function invertImg(){
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d")
	
	// This ensures the data is in the right context
		if (!context || !context.getImageData || !context.putImageData || !context.drawImage) {
			return;
		}

		context.fillStyle = "rgb(0,200,0)";
		context.fillRect(0, 0, 300, 300);
		context.drawImage(mySrcImg, 0, 0);
	// This defines the image data
		var myImageData;
	// The image data is the collected.
		try {
			myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		} catch (e) {
			myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		}
	// This loops over every pixel set on the canvas
		var pixelComponents = myImageData.data;
		var n = pixelComponents.length;
		for (var i = 0; i < n; i ++) {
			

			pixelComponents[i*4] = 255-pixelComponents[i*4];
			pixelComponents[i*4 + 1] = 255-pixelComponents[i*4+1];
			pixelComponents[i*4 + 2] = 255-pixelComponents[i*4+2];
		}

	// This then allows the image data to be drawn back onto the canvas	
		context.putImageData(myImageData, 0, 0);
}





// This is the threshold effect function
function thresholdImg() {
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
// This ensures the data is in the right context
  	if (!context || !context.getImageData || !context.putImageData || !context.drawImage) {
    	return;
  	}	

	context.fillStyle = "rgb(0,200,0)";
 	context.fillRect (0, 0, 300, 300);
	
// This copies the data to the canvas
    context.drawImage(mySrcImg, 0, 0);
// This defines the image data	
	var myImageData;
// The image data is the collected.
	try {
      	myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
    } catch (e) {
      	netscape.security.PrivilegeManager.enablePrivilege("UniversalBrowserRead");
      	myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
    }

// This loops over every pixel set on the canvas
	var pixelComponents = myImageData.data;
	var n = pixelComponents.length;
	for (var i = 0; i < n; i += 4) {
// Then they are added together then divided	
		var average = (pixelComponents[i  ] + pixelComponents[i+1] + pixelComponents[i+2]) / 3;
		if(average > 127.5){
			average = 255;
		} else {
			average = 0;
		}
		
  		pixelComponents[i  ] = average; 
  		pixelComponents[i+1] = average; 
  		pixelComponents[i+2] = average; 
  		
	}

// This then allows the image data to be drawn back onto the canvas		
	context.putImageData(myImageData, 0,0);

}



// This is the emboss effect function 
function embossImg () {

// This is the convolution width and height.	
var convolutionWidth = 3;
var convolutionHeight = 3;

var factor = 1.0;
var bias = 128;
//This is the convolution mask 
var convolutionMask1 = new Array(convolutionHeight);
	for (i=0; i < convolutionHeight; i++){
	convolutionMask1[i]=new Array(convolutionWidth)
}

convolutionMask1[0][0] = -1.0;				convolutionMask1[1][0] = 0.0;		convolutionMask1[2][0] = 1.0;
convolutionMask1[0][1] = -1.0;				convolutionMask1[1][1] = 0.0;		convolutionMask1[2][1] = 1.0;
convolutionMask1[0][2] = -1.0;				convolutionMask1[1][2] = 0.0;		convolutionMask1[2][2] = 1.0;
// The end of the convolution mask

// This is the output of the convolution output which is width * height in size.
var convolutionKernel_OutputArraySize =((convolutionWidth * convolutionHeight) * 4);
var convolutionKernel_Output = new Array(convolutionKernel_OutputArraySize);

	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d"); 
	
	
// This copies the data to the canvas
	context.drawImage(mySrcImg, 0, 0);
	
	// This defines the image data	
	var myImageData;
	var newImageData;
	// The image data is the collected.
	try {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		newImageData = context.createImageData(canvas.width, canvas.height);
	} catch (e) {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		newImageData = context.createImageData(canvas.width, canvas.height);
	}

	
	var threshold = 100;
	var convolutionMask = convolutionMask1;
// This loops over every pixel set on the canvas	
	for (var x = 0; x < canvas.width; x++){
		for (var y = 0; y < canvas.height; y++) {
			
// This is the index
			var idx = (x + y * canvas.width) * 4;
// These are the indexes for the convolution kernel 			
			for (var filterx = 0; filterx < convolutionWidth; filterx++){
				for (var filtery = 0; filtery < convolutionHeight; filtery++){
					var tmpX = ((x - Math.floor(convolutionWidth / 2)) + filterx + canvas.width) % canvas.width;
					var tmpY = ((y - Math.floor(convolutionHeight / 2)) + filtery + canvas.height) % canvas.height;
					var convolutionKernel_Index = (tmpX + tmpY * canvas.width) * 4;
				 
					var outputIndex = (filterx + filtery * convolutionWidth) * 4;
					convolutionKernel_Output[outputIndex  ] = (convolutionMask[filterx][filtery]  *
																		myImageData.data[convolutionKernel_Index  ]);
					convolutionKernel_Output[outputIndex+1] = (convolutionMask[filterx][filtery]  *
																		myImageData.data[convolutionKernel_Index+1]);
					convolutionKernel_Output[outputIndex+2] = (convolutionMask[filterx][filtery]  *
																		myImageData.data[convolutionKernel_Index+2]);
					convolutionKernel_Output[outputIndex+3] = 255;
				}
			}
			
	// This then loops through the output values 				
	var newPixel = new Array(4);
	for (i=0; i < 4; i++){
		newPixel[i] = 0; 
	}
	
	for (i=0; i < convolutionKernel_OutputArraySize; i+=4){
		newPixel[0] = newPixel[0] + convolutionKernel_Output[i  ];
		newPixel[1] = newPixel[1] + convolutionKernel_Output[i+1];
		newPixel[2] = newPixel[2] + convolutionKernel_Output[i+2];
		newPixel[3] = newPixel[3] + convolutionKernel_Output[i+3];
	}
	
	var avg = newPixel[0] + newPixel[1] + newPixel[2] / 3;
	newPixel[0] = avg;
	newPixel[1] = avg;
	newPixel[2] = avg;
// This sets the new pixel colour	
	newImageData.data[idx  ] = Math.min( Math.max( (newPixel[0] * factor + bias), 0), 255);
	newImageData.data[idx+1] = Math.min( Math.max( (newPixel[1] * factor + bias), 0), 255);
	newImageData.data[idx+2] = Math.min( Math.max( (newPixel[2] * factor + bias), 0), 255);
	newImageData.data[idx+3] = Math.min( Math.max( (newPixel[3] * factor + bias), 0), 255);
			

	}
}
// This then allows the image data to be drawn back onto the canvas
context.putImageData(newImageData, 0, 0);
}


// This is the sharpen effect function
function sharpenImg (){


// This is the convolution width and height.
var convolutionWidth = 3;
var convolutionHeight = 3;

var factor = 1.0;
var bias = 0.0;
//This is the convolution mask 
var convolutionMask1 = new Array(convolutionHeight);
	for (i=0; i < convolutionHeight; i++){
	convolutionMask1[i]=new Array(convolutionWidth)
}

convolutionMask1[0][0] = -1.0;		convolutionMask1[1][0] = -1.0;		convolutionMask1[2][0] = -1.0;
convolutionMask1[0][1] = -1.0;		convolutionMask1[1][1] = 9.0;		convolutionMask1[2][1] = -1.0;
convolutionMask1[0][2] = -1.0;		convolutionMask1[1][2] = -1.0;		convolutionMask1[2][2] = -1.0;
// The end of the convolution mask

// This is the output of the convolution output which is width * height in size.
var convolutionKernel_OutputArraySize =((convolutionWidth * convolutionHeight) * 4);
var convolutionKernel_Output = new Array(convolutionKernel_OutputArraySize);

	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d"); 
	
	
// This copies the data to the canvas	
	context.drawImage(mySrcImg, 0, 0);
	
	// This defines the image data		
	var myImageData;
	var newImageData;
	// The image data is the collected.	
	try {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		newImageData = context.createImageData(canvas.width, canvas.height);
	} catch (e) {
		myImageData = context.getImageData(0, 0, canvas.width, canvas.height);
		newImageData = context.createImageData(canvas.width, canvas.height);
	}
	
	var threshold = 100;
	var convolutionMask = convolutionMask1;
// This loops over every pixel set on the canvas	
	for (var x = 0; x < canvas.width; x++){
		for (var y = 0; y < canvas.height; y++) {
// This is the index			
			var idx = (x + y * canvas.width) * 4;
// These are the indexes for the convolution kernel 			
			for (var filterx = 0; filterx < convolutionWidth; filterx++){
			for (var filtery = 0; filtery < convolutionHeight; filtery++){
				 var tmpX = ((x - Math.floor(convolutionWidth / 2)) + filterx + canvas.width) % canvas.width;
				 var tmpY = ((y - Math.floor(convolutionHeight / 2)) + filtery + canvas.height) % canvas.height;
				 var convolutionKernel_Index = (tmpX + tmpY * canvas.width) * 4;
				 
				 var outputIndex = (filterx + filtery * convolutionWidth) * 4;
				 convolutionKernel_Output[outputIndex  ] = (convolutionMask[filterx][filtery]	* factor)  *
																		myImageData.data[convolutionKernel_Index  ];
				 convolutionKernel_Output[outputIndex+1] = (convolutionMask[filterx][filtery]	* factor)  *
																		myImageData.data[convolutionKernel_Index+1];
				 convolutionKernel_Output[outputIndex+2] = (convolutionMask[filterx][filtery]	* factor)  *
																		myImageData.data[convolutionKernel_Index+2];
				 convolutionKernel_Output[outputIndex+3] = 255;
			
	// This then loops through the output values 			
	var newPixel = new Array(4);
	for (i=0; i < 4; i++){
		newPixel[i] = 0; 
	}
	
	for (i=0; i < convolutionKernel_OutputArraySize; i+=4){
		newPixel[0] = newPixel[0] + convolutionKernel_Output[i  ] + bias;
		newPixel[1] = newPixel[1] + convolutionKernel_Output[i+1] + bias;
		newPixel[2] = newPixel[2] + convolutionKernel_Output[i+2] + bias;
		newPixel[3] = newPixel[3] + convolutionKernel_Output[i+3] + bias;
	}
// This sets the new pixel colour
	newImageData.data[idx  ] = Math.min( Math.max( (newPixel[0] * factor + bias), 0), 255);
	newImageData.data[idx+1] = Math.min( Math.max( (newPixel[1] * factor + bias), 0), 255);
	newImageData.data[idx+2] = Math.min( Math.max( (newPixel[2] * factor + bias), 0), 255);
	newImageData.data[idx+3] = Math.min( Math.max( newPixel[3], 0), 255);	

	}
}
// This then allows the image data to be drawn back onto the canvas
context.putImageData(newImageData, 0, 0);
}
	
	}
	}
	
// This function controls what effect is to be applied and when to be applied	
function applyFilter(filterType){
	srcImg();
	// This is the switch that allows the user to control and choose between effects
	switch (filterType) {
		case 'greyscale': 
			greyImg();
			break;
		case 'blur':
			blurImg();
			break;	
		case 'hue':
			hueImg();      
			break;
		case 'invert':
			invertImg();
			break;
		case 'threshold':
			thresholdImg();
			break;
		case 'emboss':
			embossImg();
			break;
		case 'sharpen':
			sharpenImg();
			break;
		default:
			break;

	}
	
}

// These global variables help the slide show to function
var i = 0; 
var image = new Array();      
var k = image.length-1;    

var slideshowImages = [];
// This function allows images to be added to the slide show 
function addtoSlideshow(){
var canvas = document.getElementById("canvas");
var context = canvas.getContext("2d");
console.log(context.getImageData(0, 0, canvas.width, canvas.height));
slideshowImages.push(context.getImageData(0, 0, canvas.width, canvas.height));
	swapImage();
}

// This function allows images to be displayed in a sequence 
function swapImage(){ 
 for (var x = 0; x < slideshowImages.length-1; x++){
	  setInterval (function(){
 console.log(slideshowImages);
 var canvas = document.getElementById("slide");
 var context = canvas.getContext("2d"); 
 context.putImageData(slideshowImages[x], 0, 0);
	  }, 3000);
 }
} 

// This allows the images to be displayed onto the slide show
function addLoadEvent(func) { 
var oldonload = window.onload; 
if (typeof window.onload != 'function') { 
window.onload = func; 
} 
else { 
window.onload = function() { 
if (oldonload) { 
oldonload(); 
} 
func();}}}  
addLoadEvent(function() { 
 }); 
 
 
 function sliderBar (){
           var result = document.getElementById('result');     
           result.innerHTML = num;
  }
 
 
 