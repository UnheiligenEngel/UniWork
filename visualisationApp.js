// myJavaScriptFile.js
// Sound Example - Simple Visualiser - A graph of the waveform / Time Domain. 

// *******************************
// Sound Visualisation variables.
// *******************************

// Set the width and height of the canvas.
var WIDTH = 200;
var HEIGHT = 125;

// Smoothing - A value from 0 -> 1 where 0 represents no time averaging with the last analysis frame. The default value is 0.8.
var SMOOTHING = 0.8;

// FFT Size - The size of the FFT used for frequency-domain analysis. Must be a power of 2.
var FFT_SIZE = 2048;

// Analyser variables.
var gainNode = null;

// Set the width and height of the spectrogram canvas visualisation.
var spectrogramCanvasWIDTH = 200;
var spectrogramCanvasHEIGHT = 125;

// The current x / draw pixel position of the spectrogram. 
var spectrogramLeftPos = 0;

// A play / stopped flag. True = play. False = stopped.
var playing = false; 

var myColor = new chroma.ColorScale({
	colors:['#0b0a0a', '#001eb3', '#00b33c','#b500c2'],
	positions:[0, .25, .75, 1],
	mode:'rgb',
	limits:[0, 255]
});
// *******************************

/* The window.requestAnimationFrame() method tells the browser that you wish to perform an animation and requests that 
the browser call a specified function to update an animation before the next repaint. The method takes as an argument 
a callback to be invoked before the repaint.
Here we use this method, or a fallback. */
window.requestAnimFrame = (function(){
	return  window.requestAnimationFrame       || 
  				window.RequestAnimationFrame || 
  				window.mozRequestAnimationFrame    || 
  				window.oRequestAnimationFrame      || 
  				window.msRequestAnimationFrame     || 
  				function( callback ){
  					window.setTimeout(callback, 1000 / 60);
				};
})();

// The analyser node.
var analyser;
var analyser2;

// Audio variables.
// *******************************

// Audio Context.
var audioContext;

// A sound.
var aSoundBuffer = null;
var soundBuffer  = null;

// A sound source.
var aSoundSource = null;
var soundSource = null;

// Add an event to the the window. The load event will call the init function.
window.addEventListener('load', init, false);

// Function to initalise the audio context.
function init() {
	try {
		// Check if the default naming is enabled, if not use the WebKit naming.
	    if (!window.AudioContext) {
	        window.AudioContext = window.webkitAudioContext;
	    }

		audioContext = new AudioContext();

		// Initalise the analyser.
		initAnalyser();
		initAnalyser2();
	}
	catch(e) {
		alert("Web Audio API is not supported in this browser");
		
  	}
}

// Add events to document elements.
function addEvents() {
	// Add an event listener to the file input.
	document.getElementById("files").addEventListener('change', loadaSound, false);
	document.getElementById("files2").addEventListener('change', loadSound, false);
	document.getElementById('volume').addEventListener('change', adjustVolume);
	document.getElementById('volume2').addEventListener('change', adjustVolume);

}

// Load a file when a file is selected.
function loadaSound(evt) {
	// Get the FileList object.
	var files = evt.target.files;

	// Get the first file in the list. 
	// This example only works with
	// the first file returned.
	var fileSelected = files[0];

    // Create a file reader.
	var reader = new FileReader();

	reader.onload = function(e) {
    	initSound(this.result);
  	};
  
	// Read in the image file as a data URL.
  	reader.readAsArrayBuffer(fileSelected);

}

// Load a file when a file is selected.
function loadSound(evt) {
	// Get the FileList object.
	var files = evt.target.files;

	// Get the first file in the list. 
	// This example only works with
	// the first file returned.
	var fileSelected = files[0];

    // Create a file reader.
	var reader = new FileReader();

	reader.onload = function(e) {
    	initSsound(this.result);
  	};
  
	// Read in the image file as a data URL.
  	reader.readAsArrayBuffer(fileSelected);

}

function adjustVolume() {
gainNode.gain.value = this.value;
}

// Initalise the sound.
function initSound(arrayBuffer) {
	audioContext.decodeAudioData(arrayBuffer, 
			function(buffer) {
				// audioBuffer is global to reuse the decoded audio later.
				aSoundBuffer = buffer;
			}, 
			function(e) {
				console.log('Error decoding file', e);
			}
		); 
}


// Initalise the sound.
function initSsound(arrayBuffer) {
	audioContext.decodeAudioData(arrayBuffer, 
			function(buffer) {
				// audioBuffer is global to reuse the decoded audio later.
				soundBuffer = buffer;
			}, 
			function(e) {
				console.log('Error decoding file', e);
			}
		); 
}


function filters(){
	var filter = audioContext.createBiquadFilter();
	aSoundSource.connect(filter);
	
	filter.connect(audioContext.destination);
	
	filter.type = 0;
	filter.frequency.value = 145;
}

function filters2(){
	var filter = audioContext.createBiquadFilter();
	soundSource.connect(filter);
	
	filter.connect(audioContext.destination);
	
	filter.type = 0;
	filter.frequency.value = 145;
}

function playO () {
	oscillator = audioContext.createOscillator();
	
	oscillator.connect(audioContext.destination);
	
	oscillator.frequency.value = 400;
	oscillator.detune.value = 0;
	oscillator.type = "sine";
	
	oscillator.start(0);
	}
	
	
function stopO() {
	if (oscillator) {
		oscillator.stop(0);	
	}
}


	
// Play the sound.
function playSound(buffer) {
	aSoundSource = audioContext.createBufferSource(); // creates a sound source.
	aSoundSource.buffer = buffer; // tell the source which sound to play.

	aSoundSource.connect(analyser); // Connect the source to the analyser.
	analyser.connect(audioContext.destination); // Connect the analyser to the context's destination (the speakers).
	
    //aSoundSource.connect(gainNode);
	
	//gainNode.connect(audioContext.destination);
	
	//var volume = document.getElementById('volume').value;
	//gainNode.gain.value = volume;
	
	aSoundSource.start(0); // play the source now.
	
		// Set the playing flag
	playing = true;
	
	// Clear the spectrogram canvas.
	var canvas = document.getElementById("canvas2");
	var context = canvas.getContext("2d");
	context.fillStyle = "rgb(255,255,255)";
	context.fillRect (0, 0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);

    // Start visualizer.
    requestAnimFrame(drawVisualisation);
}

// Play the sound.
function playTheSound(buffer) {
	soundSource = audioContext.createBufferSource(); // creates a sound source.
	soundSource.buffer = buffer; // tell the source which sound to play.

	soundSource.connect(analyser2); // Connect the source to the analyser.
	analyser2.connect(audioContext.destination); // Connect the analyser to the context's destination (the speakers).
	
	// soundSource.connect(gainNode);
	
	// gainNode.connect(audioContext.destination);
	
	// var volume = document.getElementById('volume2').value;
	// gainNode.gain.value = volume;
	
	soundSource.start(0); // play the source now.
	
		// Set the playing flag
	playing = true;
	
	// Clear the spectrogram canvas.
	var canvas = document.getElementById("canvas3");
	var context = canvas.getContext("2d");
	context.fillStyle = "rgb(255,255,255)";
	context.fillRect (0, 0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);

    // Start visualizer.
    requestAnimFrame(drawTheVisualisation);
}


// Stop the sound.
// Simple stop. Will only stop the last 
// sound if you press play more than once.
function stopSound() {
	if (aSoundSource) {
		aSoundSource.stop(0);	
	}
}

// Stop the sound.
// Simple stop. Will only stop the last 
// sound if you press play more than once.
function stopTheSound() {
	if (soundSource) {
		soundSource.stop(0);
		
	}
}



// *******************************
// Visualisation Functions.
// *******************************

// Function to initalise the analyser.
function initAnalyser() {

	analyser = audioContext.createAnalyser();
	
	analyser.smoothingTimeConstant = SMOOTHING;
	
	analyser.fftSize = 	FFT_SIZE;
}

function initAnalyser2() {

	analyser2 = audioContext.createAnalyser();
	
	analyser2.smoothingTimeConstant = SMOOTHING;
	
	analyser2.fftSize = 	FFT_SIZE;
}
// Draw the visualisation.
function drawVisualisation() {
	
	var canvas = document.getElementById("canvas");
	var context = canvas.getContext("2d");
	
	canvas.width = WIDTH;
	canvas.height = HEIGHT;
	
	context.fillStyle = "rgb(255,255,255)";
	context.fillRect (0, 0, WIDTH, HEIGHT);
	
	drawFrequencyDomainVisualisation(context);
	
	drawTimeDomainVisualisation(context);
	
	drawSpectrogramVisualisation();
	
	if(playing){
		requestAnimFrame(drawVisualisation);
	}
}

// Draw the visualisation.
function drawTheVisualisation() {
	
	var canvas = document.getElementById("canvas3");
	var context = canvas.getContext("2d");
	
	canvas.width = WIDTH;
	canvas.height = HEIGHT;
	
	context.fillStyle = "rgb(255,255,255)";
	context.fillRect (0, 0, WIDTH, HEIGHT);
	
	drawTheFrequencyDomainVisualisation(context);
	
	drawTheTimeDomainVisualisation(context);
	
	drawTheSpectrogramVisualisation();
	
	if(playing){
		requestAnimFrame(drawTheVisualisation);
	}
}



// Draw the time domain visualisation.
function drawTimeDomainVisualisation(context) {
	
	var timeDomain = new Uint8Array(analyser.frequencyBinCount);
	
	analyser.getByteTimeDomainData(timeDomain);
	
	for (var i = 0; i < analyser.frequencyBinCount; i++) {
		
		var value = timeDomain[i];
		
		var percent = value / 256;
		
		var height = HEIGHT * percent;
		var offset = HEIGHT - height - 1;
		var barWidth = WIDTH/analyser.frequencyBinCount;
		
		context.fillStyle = "black";
		
		context.fillRect(i * barWidth, offset, 1, 1);
	}


}

function drawTheTimeDomainVisualisation(context) {
	
	var timeDomain = new Uint8Array(analyser2.frequencyBinCount);
	
	analyser2.getByteTimeDomainData(timeDomain);
	
	for (var i = 0; i < analyser2.frequencyBinCount; i++) {
		
		var value = timeDomain[i];
		
		var percent = value / 256;
		
		var height = HEIGHT * percent;
		var offset = HEIGHT - height - 1;
		var barWidth = WIDTH/analyser2.frequencyBinCount;
		
		context.fillStyle = "black";
		
		context.fillRect(i * barWidth, offset, 1, 1);
	}


}


// Draw the frequency domain visualisation.
function drawFrequencyDomainVisualisation(context) {
	
	var freqDomain = new Uint8Array(analyser.frequencyBinCount);
	
	analyser.getByteFrequencyData(freqDomain);
	
	for (var i = 0; i < analyser.frequencyBinCount; i++){
	
		var value = freqDomain[i];
		
		var percent = value / 256;
		
		var height = HEIGHT * percent;
		var offset = HEIGHT - height - 1;
		var barWidth = WIDTH/analyser.frequencyBinCount;
		
		var hue = i/analyser.frequencyBinCount * 360;
		context.fillStyle = "hsl(" + hue + ", 100%, 50%)";
		
		context.fillRect(i * barWidth, offset, barWidth, height);
		
}
}

function drawTheFrequencyDomainVisualisation(context) {
	
	var freqDomain = new Uint8Array(analyser2.frequencyBinCount);
	
	analyser2.getByteFrequencyData(freqDomain);
	
	for (var i = 0; i < analyser2.frequencyBinCount; i++){
	
		var value = freqDomain[i];
		
		var percent = value / 256;
		
		var height = HEIGHT * percent;
		var offset = HEIGHT - height - 1;
		var barWidth = WIDTH/analyser2.frequencyBinCount;
		
		var hue = i/analyser2.frequencyBinCount * 360;
		context.fillStyle = "hsl(" + hue + ", 100%, 50%)";
		
		context.fillRect(i * barWidth, offset, barWidth, height);
		
}
}


function drawSpectrogramVisualisation() {
	
	var canvas = document.getElementById("canvas2");
	var context = canvas.getContext("2d");
	
	
	var tempCanvas = document.createElement("canvas");
	tempCanvas.width = spectrogramCanvasWIDTH;
	tempCanvas.height = spectrogramCanvasHEIGHT;
	var tempCtx = tempCanvas.getContext("2d");
	
	tempCtx.drawImage(canvas,0,0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);
	
	var freqDomain = new Uint8Array(analyser.frequencyBinCount);
	
	analyser.getByteFrequencyData(freqDomain);
	
	var highestValue = -1;
	
	var highestValueIndex = -1;
	
	var highestValueLength = 1;
	
	
	if(spectrogramLeftPos == spectrogramCanvasWIDTH){
		
		for (var i = 0; i < analyser.frequencyBinCount; i++){
			var value = freqDomain[i];
			
		if (value > highestValue){
			highestValue = value;
			highestValueIndex = i;
			highestValueLength = 1;
		} else {
			if (value == highestValue){
				if( ( highestValueIndex + highestValueLength) == i){
					highestValueLength++;
				}
			}
		}
			
			
		
		tempCtx.fillStyle = myColor.getColor(value).hex();
		tempCtx.fillRect(spectrogramCanvasWIDTH - 1, (spectrogramCanvasHEIGHT - i), 1, 1);
		
		}
		
	context.translate(-1, 0);
	context.drawImage(tempCanvas,0,0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);
	
	context.setTransform(1, 0, 0, 1, 0, 0);
	
	} else {
		
		for (var i = 0; i < analyser.frequencyBinCount; i++){
			var value = freqDomain[i];
			
			if (value > highestValue){
			highestValue = value;
			highestValueIndex = i;
			highestValueLength = 1;
		} else {
			if (value == highestValue){
				if( ( highestValueIndex + highestValueLength) == i){
					highestValueLength++;
				}
			}
		}
			
			
			
		tempCtx.fillStyle = myColor.getColor(value).hex();
		tempCtx.fillRect(spectrogramLeftPos, (spectrogramCanvasHEIGHT - i), 1, 1);
		}
		
		context.drawImage(tempCanvas,0,0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);
		
		spectrogramLeftPos++;
		
		}
		
	var highestValIdxStart = highestValueIndex;
	var highestValIdxEnd = highestValueIndex + (highestValueLength - 1);
	var tempIndex = Math.round( (highestValIdxStart + highestValIdxEnd) / 2);
	
	var tmpFreq = getValueToFrequency(tempIndex);
	var tmpIndex = getFrequencyToIndex(tmpFreq);
	document.getElementById("debugInfo").innerHTML="freqDomain.length: " + freqDomain.length +
										" / highestValue " + highestValue +
										" / highestValueIndex: " + highestValueIndex +
										" / highestValueLength: " + highestValueLength +
										" |----| highestValueLength AS INDEX: " +
										(highestValueIndex + (highestValueLength -1)) +
										" |----| tempIndex: " + tempIndex +
										" |----| getValueToFrequency: " + tmpFreq +
										" / getFrequencyToIndex: " + tmpIndex + "\n";
		
		
	}

function drawTheSpectrogramVisualisation() {
	
	var canvas = document.getElementById("canvas4");
	var context = canvas.getContext("2d");
	
	
	var tempCanvas = document.createElement("canvas");
	tempCanvas.width = spectrogramCanvasWIDTH;
	tempCanvas.height = spectrogramCanvasHEIGHT;
	var tempCtx = tempCanvas.getContext("2d");
	
	tempCtx.drawImage(canvas,0,0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);
	
	var freqDomain = new Uint8Array(analyser2.frequencyBinCount);
	
	analyser2.getByteFrequencyData(freqDomain);
	
	var highestValue = -1;
	
	var highestValueIndex = -1;
	
	var highestValueLength = 1;
	
	
	if(spectrogramLeftPos == spectrogramCanvasWIDTH){
		
		for (var i = 0; i < analyser2.frequencyBinCount; i++){
			var value = freqDomain[i];
			
		if (value > highestValue){
			highestValue = value;
			highestValueIndex = i;
			highestValueLength = 1;
		} else {
			if (value == highestValue){
				if( ( highestValueIndex + highestValueLength) == i){
					highestValueLength++;
				}
			}
		}
			
			
		
		tempCtx.fillStyle = myColor.getColor(value).hex();
		tempCtx.fillRect(spectrogramCanvasWIDTH - 1, (spectrogramCanvasHEIGHT - i), 1, 1);
		
		}
		
	context.translate(-1, 0);
	context.drawImage(tempCanvas,0,0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);
	
	context.setTransform(1, 0, 0, 1, 0, 0);
	
	} else {
		
		for (var i = 0; i < analyser2.frequencyBinCount; i++){
			var value = freqDomain[i];
			
			if (value > highestValue){
			highestValue = value;
			highestValueIndex = i;
			highestValueLength = 1;
		} else {
			if (value == highestValue){
				if( ( highestValueIndex + highestValueLength) == i){
					highestValueLength++;
				}
			}
		}
			
			
			
		tempCtx.fillStyle = myColor.getColor(value).hex();
		tempCtx.fillRect(spectrogramLeftPos, (spectrogramCanvasHEIGHT - i), 1, 1);
		}
		
		context.drawImage(tempCanvas,0,0, spectrogramCanvasWIDTH, spectrogramCanvasHEIGHT);
		
		spectrogramLeftPos++;
		
		}
		
	var highestValIdxStart = highestValueIndex;
	var highestValIdxEnd = highestValueIndex + (highestValueLength - 1);
	var tempIndex = Math.round( (highestValIdxStart + highestValIdxEnd) / 2);
	
	var tmpFreq = getTheValueToFrequency(tempIndex);
	var tmpIndex = getTheFrequencyToIndex(tmpFreq);
	document.getElementById("debugIinfo").innerHTML="freqDomain.length: " + freqDomain.length +
										" / highestValue " + highestValue +
										" / highestValueIndex: " + highestValueIndex +
										" / highestValueLength: " + highestValueLength +
										" |----| highestValueLength AS INDEX: " +
										(highestValueIndex + (highestValueLength -1)) +
										" |----| tempIndex: " + tempIndex +
										" |----| getTheValueToFrequency: " + tmpFreq +
										" / getTheFrequencyToIndex: " + tmpIndex + "\n";
		
		
	}	
	

function getValueToFrequency(tmpValue) {
	
	var nyquistFrequency = audioContext.sampleRate / 2;
	
	var freq = tmpValue * nyquistFrequency / analyser.frequencyBinCount;
		
	
	return freq;
}

function getTheValueToFrequency(tmpValue) {
	
	var nyquistFrequency = audioContext.sampleRate / 2;
	
	var freq = tmpValue * nyquistFrequency / analyser2.frequencyBinCount;
		
	
	return freq;
}


function getFrequencyToIndex(freq) {
	
	var nyquistFrequency = audioContext.sampleRate / 2;
	
	var index = Math.round(freq / nyquistFrequency * analyser.frequencyBinCount);
	
	
	return index;
}


function getTheFrequencyToIndex(freq) {
	
	var nyquistFrequency = audioContext.sampleRate / 2;
	
	var index = Math.round(freq / nyquistFrequency * analyser2.frequencyBinCount);
	
	
	return index;
}

